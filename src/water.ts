
import * as math from './math';

function clamp(value:number, min:number, max:number) {
    if (value < min) {
        return min;
    } else if (value > max) {
        return max;
    }
    return value;
}

interface Droplet {
    // initiation time
    t0:number
    // global current position
    position:math.Vector
    // starting position
    initialPosition:math.Vector
    velocity:math.Vector
    // has streaklines
    streaks:boolean
}

interface Updater {
    readonly time:number
    reset():void
    update(t:number):void
    draw(ctx:CanvasRenderingContext2D):void
}

class Droplets implements Updater {
    droplets:Droplet[] = []
    private _time:number
    get time():number {
        return this._time;
    }
    constructor(){
        this.reset();
    }
    reset() {
        this.droplets = [];
        this._time = 0;
    }
    update(t:number) {
        this.droplets.forEach((d)=>{
            d.position = d.initialPosition.add(d.velocity.multiply(t-d.t0));
        });
    }
    draw(ctx:CanvasRenderingContext2D) {
        ctx.beginPath();
        ctx.strokeStyle = "rgba(0,0,255,0.4)";
        ctx.fillStyle = "rgba(0,0,255,0.2)";
        ctx.lineWidth = 1;
        let r = 2;
        this.droplets.forEach((d)=>{
            if (d.streaks) {
                ctx.moveTo(...d.initialPosition.value());
                ctx.lineTo(...d.position.value());
            }
            ctx.moveTo(d.position.get(0) + r, d.position.get(1));
            ctx.arc(d.position.get(0), d.position.get(1), r, 0, 2*Math.PI);
        });
        // if (this.drawPath && this.droplets.length > 0) {
        //     ctx.moveTo(...this.droplets[0].position.value());
        //     this.droplets.forEach((d)=>{
        //         ctx.lineTo(...d.position.value());
        //     });
        //     ctx.stroke();
        // }
        ctx.stroke();
        ctx.fill();
    }
}

interface InternalWaterer {
    // initial angular position
    th0:number
    // arm length
    r:number
    // angular velocity
    w:number
    // water exit velocity
    h20_output:number
    // water exit angle
    phi:number
    // current angular position
    readonly th:number

    toggle(value:boolean):void
}
export interface Waterer extends InternalWaterer {
}


interface WatererUpdater extends Updater, InternalWaterer {

    getDroplets(streaks:boolean): Droplet[]
}


class SingleWaterer implements WatererUpdater {
    protected _th:number = 0
    protected _th0:number = 0
    protected _r:number = 1
    protected _w:number = 1
    protected _h20_output:number = 1
    protected _phi:number = 0
    get th():number {
        return this._th;
    }
    get th0():number {
        return this._th0;
    }
    set th0(v:number) {
        this._th += v - this._th0;
        this._th0 = v;
    }
    get r():number {
        return this._r;
    }
    set r(v:number) {
        this._r = v;
    }
    get w():number {
        return this._w;
    }
    set w(v:number) {
        this._w = v;
    }
    get h20_output():number {
        return this._h20_output;
    }
    set h20_output(v:number) {
        this._h20_output = v;
    }
    get phi():number {
        return this._phi;
    }
    set phi(v:number) {
        this._phi = v;
    }
    protected _time:number
    get time() {
        return this._time;
    }

    private paused:boolean
    constructor() {
        this.th0 = 0;
        this.r = 100;
        this.w = 1;
        this.h20_output = 40;
        this.phi = Math.PI/4;
    }
    private point_timeout:any
    reset() {
        this._th = this.th0;
        this.paused = false;
    }
    draw(ctx:CanvasRenderingContext2D) {
        ctx.beginPath();
        ctx.moveTo(0,0);
        ctx.strokeStyle = "rgba(100,100,100,1)";
        ctx.lineWidth = 10;
        ctx.lineJoin = "round";

        ctx.beginPath();
        ctx.moveTo(...this.p0.value());
        ctx.lineTo(...this.p1.value());
        ctx.lineTo(...this.p2.value());
        ctx.stroke();

        ctx.beginPath();
        let offset = this.p2.subtract(this.p1).rotate(Math.PI/2).normalize().multiply(ctx.lineWidth/2);
        let start = this.p2.add(offset);
        let end = this.p2.subtract(offset);
        ctx.strokeStyle = "rgba(0,0,0,1)";
        ctx.lineWidth = 3;
        ctx.moveTo(...start.value());
        ctx.lineTo(...end.value());
        ctx.stroke();

    }
    // waterer arm described by three points
    private p0:math.Vector  // arm pivot point (0,0)
    private p1:math.Vector  // end of main arm
    private p2:math.Vector  // end of secondary arm
    private v2:math.Vector  // exit velocity of water

    update(t:number) {
        this._time = this._time || t;
        let dt = t - this._time;
        this._time = t;
        this.p0 = this.p0 || new math.Vector([0, 0]);
        let w = this.w;
        this._th += this.w*dt;
        let a = math.Matrix.getRotationMatrix(this._th);
        let b = math.Matrix.getRotationMatrix(this.phi);
        let r = new math.Vector([0, -this.r]);
        this.p1 = a.inv().multiply(r);
        let small_arm = this.p1.multiply(0.1).rotate(this.phi);
        this.p2 = this.p1.add(small_arm);
        // exit velocity due to rotation
        let velocity_rotation = this.p2.rotate(Math.PI/2).multiply(w);
        let velocity_tangential = small_arm.normalize().multiply(this.h20_output);
        this.v2 = velocity_rotation.add(velocity_tangential);
    }
    toggle(v:boolean) {

    }
    getDroplets(streaks:boolean):Droplet[] {
        return [{
            t0: this.time,
            initialPosition: this.p2,
            position: this.p2,
            velocity: this.v2,
            streaks:streaks,
        }];
    }
}

class MultiWaterer extends SingleWaterer {
    children:SingleWaterer[] = []
    set th0(v:number) {
        let diff = v - this.th0;
        (this.children || []).forEach((w)=>{w.th0 += diff});
        this._th0 = v;
    }
    set r(v:number) {
        (this.children || []).forEach((w)=>{w.r = v});
        this._r = v;
    }
    set w(v:number) {
        (this.children || []).forEach((w)=>{w.w = v});
        this._w = v;
    }
    set h20_output(v:number) {
        (this.children || []).forEach((w)=>{w.h20_output = v});
        this._h20_output = v;
    }
    set phi(v:number) {
        (this.children || []).forEach((w)=>{w.phi = v});
        this._phi = v;
    }
    get th0() {
        return this._th0;
    }
    get r() {
        return this._r;
    }
    get w() {
        return this._w;
    }
    get h20_output() {
        return this._h20_output;
    }
    get phi() {
        return this._phi;
    }
    constructor() {
        super()
    }


    update(t:number) {
        this._time = this._time || t;
        this._th += this.w*(t - this._time);
        this._time = t;
        this.children.forEach((u)=>{u.update(t)});
    }
    reset() {
        this.children.forEach((u)=>{u.reset()});
    }
    draw(ctx:CanvasRenderingContext2D) {
        this.children.forEach((u)=>{u.draw(ctx)});
    }
    getDroplets(streaks:boolean) {
        let ret:Droplet[] = [];
        this.children.forEach((u)=>{u.getDroplets(streaks).forEach((d)=>{ret.push(d);});});
        return ret;
    }
}

export class SedAnimator {
    dt:number = 0.1
    private offsetX:number = 0
    private offsetY:number = 0
    private scale:number = 1
    private div:HTMLDivElement
    private canvas:HTMLCanvasElement
    private ctx:CanvasRenderingContext2D
    private _waterer:MultiWaterer = new MultiWaterer()
    private t:number = 0
    private t0:number|undefined = undefined
    private _generateStreaks:boolean = false

    droplets:Droplets = new Droplets()

    get waterer():Waterer {
        return this._waterer;
    }
    clearWater() {
        this.droplets = new Droplets();
    }
    clearStreaks() {
        this.droplets.droplets.forEach((d)=>{
            d.streaks = false;
        });
    }
    generateStreaks(v:boolean) {
        this._generateStreaks = v;
    }
    constructor(div:HTMLDivElement) {
        this.div = div;
        this.canvas = document.createElement("canvas");
        this.canvas.style.width = '100%';
        this.canvas.style.height = '100%';
        this.offsetX = this.div.offsetWidth / 2;
        this.offsetY = this.div.offsetHeight / 2;
        while (div.children.length > 0) {
            div.children.item(0).remove();
        }
        div.appendChild(this.canvas);
        this.ctx = this.canvas.getContext("2d");
        this.attachInputs({div:this.div});
    }
    private get updaters():Updater[] {
        return [this._waterer, this.droplets];
    }
    private update(t:number) {
        while (this.t + this.dt < t) {
            this._waterer.update(this.t + this.dt);
            this._waterer.getDroplets(this._generateStreaks).forEach((d)=>{
                this.droplets.droplets.push(d);
            })
            this.t += this.dt;
        }
        while (this.droplets.droplets.length > 1000) {
            this.droplets.droplets.shift();
        }
        this.updaters.forEach((u)=>{u.update(t)});
    }
    private draw() {
        this.canvas.width = this.div.offsetWidth;
        this.canvas.height = this.div.offsetHeight;
        this.offsetX = clamp(this.offsetX, 0, this.canvas.width);
        this.offsetY = clamp(this.offsetY, 0, this.canvas.height);
        this.ctx.resetTransform();
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx.fillStyle = "white";
        this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx.translate(this.offsetX, this.offsetY);
        this.ctx.scale(this.scale, -this.scale);
        this.updaters.forEach((u)=>{u.draw(this.ctx)});
    }
    reset() {
        this.updaters.forEach((u)=>{u.reset()});
    }
    animate() {
        requestAnimationFrame((t: number)=>{
            t /= 1000;
            if (this.t0 === undefined) {
                this.t0 = t;
            }
            this.update(t - this.t0);
            this.draw();
            this.animate();
        });
    }
    setWatererCount(n:number) {
        while (this._waterer.children.length > 0) {
            this._waterer.children.pop();
        }
        let pos = 0;
        for (let i=0; i < n; i++) {
            let w = new SingleWaterer();
            w.th0 = pos;
            this._waterer.children.push(w);
            pos += 2* Math.PI / n;
        }
    }
    attachInputs(opt:Partial<{
        omega:HTMLInputElement,
        radius:HTMLInputElement,
        phi:HTMLInputElement,
        flowrate:HTMLInputElement,
        div:HTMLDivElement,
    }>) {
        if (opt.div !== undefined) {
            opt.div.addEventListener('pointerdown', (e)=>{
                if (e.buttons === 1) {
                    opt.div.setPointerCapture(e.pointerId);
                }
            });
            opt.div.addEventListener('pointerup', (e)=>{
                opt.div.releasePointerCapture(e.pointerId);
            });
            opt.div.addEventListener('pointermove', (e)=>{
                if (e.buttons === 1) {
                    this.offsetX += e.movementX;
                    this.offsetY += e.movementY;
                }
            });
            opt.div.addEventListener('wheel', (e)=>{
                this.scale += e.deltaY / 100;
                if (this.scale < 0.1) {
                    this.scale = 0.1;
                } else if (this.scale > 10) {
                    this.scale = 10;
                }
            });
        }
        if (opt.flowrate !== undefined) {
            opt.flowrate.value = String(this._waterer.h20_output);
            ['input', 'change'].forEach((s)=>{
                opt.flowrate.addEventListener(s, (e)=>{
                    this._waterer.h20_output = parseFloat(opt.flowrate.value);
                });
            });
        }
        if (opt.omega !== undefined) {
            opt.omega.value = String(this._waterer.w);
            ['input', 'change'].forEach((s)=>{
                opt.omega.addEventListener(s, (e)=>{
                    this._waterer.w = parseFloat(opt.omega.value);
                });
            });
        }
        if (opt.radius !== undefined) {
            opt.radius.value = String(this._waterer.r);
            ['input', 'change'].forEach((s)=>{
                opt.radius.addEventListener(s, (e)=>{
                    this._waterer.r = parseFloat(opt.radius.value);
                });
            });
        }
        if (opt.phi !== undefined) {
            opt.phi.value = String(this._waterer.phi);
            ['input', 'change'].forEach((s)=>{
                opt.phi.addEventListener(s, (e)=>{
                    this._waterer.phi = parseFloat(opt.phi.value);
                });
            });
        }
    }
}


export function init(div: HTMLDivElement):SedAnimator {

    let w = new SedAnimator(div);
    w.setWatererCount(3);
    w.animate();

    return w;
}

