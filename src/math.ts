export class Vector {
    private _value:[number, number]
    constructor(a:[number, number]) {
        this._value = [a[0], a[1]];
    }
    add(other:Vector): Vector {
        return new Vector([this._value[0] + other.get(0), this._value[1] + other.get(1)]);
    }
    subtract(other:Vector): Vector {
        return this.add(other.multiply(-1));
    }
    get(index:0|1) {
        return this._value[index];
    }
    multiply(other:number): Vector {
        return new Vector([this._value[0] * other, this._value[1] * other]);
    }
    value():[number, number] {
        return [this._value[0], this._value[1]];
    }
    norm(): number {
        let x = this._value[0];
        let y = this._value[1];
        return Math.sqrt(x*x+y*y);
    }
    normalize(): Vector {
        try {
            return this.multiply(1/this.norm());
        } catch(e) {
            if (this._value[0] === 0 && this._value[1] === 0) {
                return new Vector([0,0]);
            }
            throw e;
        }
    }
    rotate(angle:number): Vector {
        let x = this._value[0];
        let y = this._value[1];
        let th = Math.atan2(y,x);
        let r = Math.sqrt(x*x+y*y);
        th += angle;
        return new Vector([Math.cos(th), Math.sin(th)]).multiply(r);
    }
}

// export class Point {
//     private cs:CoordinateSystem|undefined
//     private position:Vector
//     constructor(a:[number,number], cs?:CoordinateSystem) {
//         this.position = new Vector(a);
//         this.cs = cs;
//     }
//     toGlobal():Point {
//         if (this.cs === undefined) {
//             return this.clone();
//         } else {
//             return this.cs.toGlobal(this);
//         }
//     }
//     // add(p:Point):Point {

//     // }
//     get x():number {
//         return this.position.get(0);
//     }
//     get y():number {
//         return this.position.get(1);
//     }
//     clone(): Point {
//         return new Point(this.position.value(), this.cs);
//     }
// }

// export class CoordinateSystem {
//     private origin:Point
//     private transform:Matrix
//     constructor(origin?:Point) {
//         this.origin = origin || new Point([0,0]);
//         this.transform = new Matrix([[1,0],[0,1]]);
//     }
//     toGlobal(p:Point): Point {
//         let origin = this.origin.toGlobal();
//         let originv = new Vector([origin.x, origin.y]);
//         let vec = this.transform.inv().multiply(new Vector([p.x, p.y]));
//         let result = originv.add(vec);
//         return new Point(result.value());
//     }
//     rotate(phi:number) {
//         this.apply(CoordinateSystem.fromRotation(phi).transform);
//     }
//     private apply(m:Matrix) {
//         this.transform = this.transform.multiply(m);
//     }
//     static fromRotation(phi:number): CoordinateSystem {
//         let ret = new CoordinateSystem();
//         ret.transform = new Matrix(
//             [[ Math.cos(phi), Math.sin(phi)],
//              [-Math.sin(phi), Math.cos(phi)]]);
//         return ret;
//     }
// }

export class Matrix {
    private _value:[[number, number],[number,number]]
    constructor(a:[[number, number],[number,number]]) {
        this._value = [[a[0][0], a[0][1]], [a[1][0], a[1][1]]];
    }
    inv():Matrix {
        let a = this._value[0][0];
        let b = this._value[0][1];
        let c = this._value[1][0];
        let d = this._value[1][1];
        let e = 1 / (a*d-b*c);
        return new Matrix([[d, -b], [-c, a]]).multiply(e);
    }
    // multiply(other:Matrix):Matrix
    multiply(other:Vector):Vector
    multiply(other:number):Matrix
    multiply(other:Vector|number):Vector|Matrix {
        if (typeof other === 'number') {
            let ret = new Matrix(this._value);
            ret._value[0][0] *= other;
            ret._value[0][1] *= other;
            ret._value[1][0] *= other;
            ret._value[1][1] *= other;
            return ret;
        } else {
            let a = this._value[0][0] * other.get(0) + this._value[0][1] * other.get(1);
            let b = this._value[1][0] * other.get(0) + this._value[1][1] * other.get(1);
            return new Vector([a,b]);
        }
    }
    get(i:0|1, j:0|1):number{
        return this._value[i][j];
    }
    clone():Matrix {
        return new Matrix(this._value);
    }
    static getRotationMatrix(th:number) {
        return new Matrix(
            [[ Math.cos(th), Math.sin(th)],
             [-Math.sin(th), Math.cos(th)]]);
    }
}
