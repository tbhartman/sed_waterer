SED Waterer
===========

A simulation of [SmarterEveryDay's sprinkler](https://youtu.be/M4-L8UgPkOk).

Kinematics only given tangential exit velocity.

Started via [Microsoft's TS starter repo](https://github.com/microsoft/TypeScript-Node-Starter).
