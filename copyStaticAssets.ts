import * as shell from "shelljs";

shell.cp("-R", "src/assets/js/lib", "dist/assets/js/");
shell.cp("-R", "src/assets/font", "dist/assets/");
shell.cp("-R", "src/assets/img", "dist/assets/");
