import * as math from "../src/math";

describe("vector", () => {
    let a = new math.Vector([1,2]);
    let b = new math.Vector([3,4]);
    it("creation", () => {
        expect(a.value()[0]).toBe(1);
        expect(a.value()[1]).toBe(2);
    });
    it("addition", () => {
        let c = a.add(b);
        expect(c.value()).toEqual([4,6]);
    });
    it("multiplication", () => {
        let c = a.multiply(2);
        expect(c.value()).toEqual([2,4]);
    });
});
describe("matrix", () => {
    let a = new math.Matrix([[1,2],[3,4]]);
    let b = new math.Vector([1,2]);
    it("creation", () => {
        expect(a.get(0,0)).toBe(1);
        expect(a.get(0,1)).toBe(2);
        expect(a.get(1,0)).toBe(3);
        expect(a.get(1,1)).toBe(4);
    });
    it("multiplication", () => {
        let c = a.multiply(2);
        expect(c.get(0,0)).toEqual(2);
        expect(c.get(0,1)).toEqual(4);
        expect(c.get(1,0)).toEqual(6);
        expect(c.get(1,1)).toEqual(8);
    });
    it("multiplication", () => {
        let c = a.multiply(b);
        expect(c.get(0)).toEqual(5);
        expect(c.get(1)).toEqual(11);
    });
    it("inversion", () => {
        let c = a.inv().multiply(2);
        expect(c.get(0,0)).toEqual(-4);
        expect(c.get(0,1)).toEqual(2);
        expect(c.get(1,0)).toEqual(3);
        expect(c.get(1,1)).toEqual(-1);
    });
});
